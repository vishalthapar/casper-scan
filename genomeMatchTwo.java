
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class genomeMatchTwo {
	public  static int forwardMismatch;
	public static int revMismatch;
	public static void main(String[] args) {
	if(args.length !=4)
	{
		System.out.println("######## ERROR IN USAGE FORMAT ####");
		System.out.println("Usage is:   ");
		System.out.println("java -Xmx2048m genomeMatchTwo <Two Part String Sequence to search with NN chars as separators> <Genome.fasta> <outputFile> <allowedNumberOfMismatches>");
		System.out.println("Eg: java -Xmx2048m genomeMatchTwo CCNGGCGGCGCTCGTACTTCTGGNNNNNNNNNNNNNNTGGCAGGTGAGTGAGGCTGCNGG hg19.fasta APC_14_out.txt 16");
		System.out.println("Exiting now. Please run again");
		System.out.println("######## ERROR IN USAGE FORMAT ####");
		System.exit(0);
	}
	else
	{
		String sequence = args[0];
		sequence = sequence.toUpperCase();
		String revSeq = revcomplement(sequence);
		String genome = args[1];
		String outfile = args[2];
		String mis = args[3];
		int mismatchNumber=0;
		try{
			mismatchNumber = Integer.parseInt(mis);
		}
		catch(NumberFormatException e)
		{
			System.out.println("######## ERROR IN USAGE FORMAT ####");
			System.out.println("Usage is:   ");
			System.out.println("java -Xmx2048m genomeMatchTwo <Two Part String Sequence to search with NN chars as separators> <Genome.fasta> <outputFile> <allowedNumberOfMismatches>");
			System.out.println("Eg: java -Xmx2048m genomeMatchTwo CCNGGCGGCGCTCGTACTTCTGGNNNNNNNNNNNNNNTGGCAGGTGAGTGAGGCTGCNGG hg19.fasta APC_14_out.txt 16");
			System.out.println("The last input must be an integer for the number of mismatches. Please rerun");
			e.printStackTrace();
			System.out.println("######## ERROR IN USAGE FORMAT ####");
			System.exit(0);
		}
		
		BufferedReader br;
		BufferedWriter bw;
		try {
				br = new BufferedReader(new FileReader(genome));
				bw = new BufferedWriter(new FileWriter(outfile));
				//System.out.println("outfile "+outfile);
				int startGene = outfile.indexOf("/");
				int endGene = outfile.indexOf("_");
				// Added in order to remove string index exception
				String geneName ="";
				String space ="";
				if(startGene >= endGene)
				{
					 geneName+="TestGene";
					 space+=mis;
				}
				else
				{
					geneName+=outfile.substring(startGene+1, endGene);
					space+=outfile.substring(endGene+1, endGene+3);
				}
				String line;
				String line1="";
				String Id="";
				int i=0;
				while((line= br.readLine())  != null)
				{ 
					if(line.contains(">"))
					{
						i=0;
						Id = line.substring(1);
						//System.out.println("contains >");
						line1="";
						
					}
					else if(!(line.contains(">")) && (line.length() < sequence.length()))
					{
						//System.out.println("going nowhere less "+line);
						line1+=line;
					}
					
					if(line1.length()>=sequence.length())
					{
							//System.out.println("greater");
							for(int k=0;k<(line1.length()-sequence.length()+1);k++)
							{
								//System.out.println("k is "+k);
								String geneStr = line1.substring(k, k+sequence.length());
								boolean match = findnewMatches(sequence,geneStr,revSeq, mismatchNumber);
								if(match)
								{
									try
									{
										if(genomeMatchTwo.forwardMismatch <=16)
										{
											String outline = geneName+"\t"+sequence.toUpperCase()+"\t"+space+"\t"+Id+"\t"+(i+k)+"\t"+"+"+"\t"+genomeMatchTwo.forwardMismatch+"\t"+geneStr.toUpperCase()+ "\n";
											bw.write(outline);
										}
										else if(genomeMatchTwo.revMismatch <=16)
										{ 
											String outline = geneName+"\t"+sequence.toUpperCase()+"\t"+space+"\t"+Id+"\t"+(i+k)+"\t"+"-"+"\t"+genomeMatchTwo.revMismatch+"\t"+geneStr.toUpperCase()+ "\n";
											bw.write(outline);
										}
									}
									catch(Exception e){
										e.printStackTrace();
									}		
								}
							}
							
							i+=line1.length()-sequence.length()+1;
							line1=line1.substring(line1.length()-sequence.length()+1);
					}	
				}
				br.close();
				bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

public static boolean findnewMatches(String seq, String geneStr, String revSeq, int mismatches)
{
	//int i =0;
	//System.out.println("line is <"+line+">\n"+"\t ID is <"+ID+">"+posChr);
	//while ( i<(line.length()-seq.length()+1))
//	{
		//String geneStr = line.substring(i, i+seq.length());
		char[] stringA = seq.toCharArray();
		char[] stringRevA = revSeq.toCharArray();
		char[] stringB = (geneStr.toUpperCase()).toCharArray(); 
		int ctr=0;
		int revCtr = 0;  
		//System.out.println("String seq is "+seq);
	//	System.out.println("String revSeq "+revSeq);		
		//System.out.println("String gen is "+geneStr);
		if(stringA.length == stringB.length){
		    for(int k = 0; k<stringA.length; k++){
		        if(stringA[k] != stringB[k] && stringA[k] !='N'){
		           ctr++;
		        } 
		        if(stringRevA[k] != stringB[k] && stringRevA[k] !='N'){
		        	revCtr++;
		        }
		    }
		}
		//System.out.println("ctr is <"+ctr+">"+"revctr is <"+revCtr+">");
		if (ctr <=mismatches || revCtr <=mismatches )
		{
			genomeMatchTwo.forwardMismatch = ctr;
			genomeMatchTwo.revMismatch = revCtr;
			return true;			
		}
		else 
			return false;
		 // increment i and move to the next position in line
//	} // end of while
}
public static String revcomplement(String dna)
{
	String comp = "";

	for (int k = 0; k < dna.length(); k++){
		if (dna.charAt(k) == 'A'){
			comp = comp + 'T';}

		if (dna.charAt(k) == 'T'){
			comp = comp + 'A';}

		if (dna.charAt(k) == 'C'){
			comp = comp + 'G';}

		if (dna.charAt(k) == 'G'){
			comp = comp + 'C';}
		if(dna.charAt(k)=='N'){
			comp=comp + 'N';}
	}
	String rev = "";
	for (int k = 0; k < comp.length(); k++){
	rev = comp.charAt(k) + rev;
	}
return rev;
}
}
