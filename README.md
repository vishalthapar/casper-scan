README
This repository is to share the code for CASPR mimatch search
feel free to email vishalthapar@gmail.com if you have any 
questions.

This is the repository for code to implement a genome-wide sliding-window search for 
potential dimeric off-target sites of CRISPR RNA-guided FokI-nucleases (RFNS) as described in 
Tsai et. al, Nature Biotechnology 2014.  

Prerequisites: 

In order to run this program, you need to have Java SDK installed on your machine or the server you run the program on. While this is installed under most MAC systems as well as linux systems, you can get it from http://www.oracle.com/technetwork/java/javase/downloads/index.html?ssSourceSiteId=otnjp
or just google "Install Java SDK on MAC OS X or Windows or Linux" (put one OS not all 3) and follow the steps
 
After installing Java, please launch the command prompt (windows) or the Shell prompt under OSx and Linux. Windows: Start-->run-->Command or in MAC the shell is under utilities in the Applications folder.
Make sure you create your working directory where you want the program to reside: 

$ mkdir /home/working/directory/

Go to that directory: 

$ cd /home/working/directory/

Next Copy the files from bitbucket to the working directory

$ cp /path/to/downloaded/files/genomeMatchTwo.* /home/working/directory/

Once there you can either compile the .java file by typing: 

$ javac genomeMatchTwo.java 
or use the precompiled file genomeMatchTwo as described below:

Example usage:
Usage is:   

$ java -Xmx2048m genomeMatchTwo <Two Part String Sequence to search with NN chars as separators> <Genome.fasta> <outputFile> <allowedNumberOfMismatches>
Eg: java -Xmx2048m genomeMatchTwo CCNGGCGGCGCTCGTACTTCTGGNNNNNNNNNNNNNNTGGCAGGTGAGTGAGGCTGCNGG hg19.fasta APC_14_out.txt 16

You can also check out the source directly from bitbucket by using the "Clone" link on this 
site and using that in the Shell window. If uncertain, google "How to clone a bitbucket 
repository in Windows, OSx or Linux" (pick one OS that you are using) 

The -Xmx2048m is telling the Java compiler to reserve at least 2 GB of memory for the program. Please make sure that the computer has that much memory or if not change the memory to a lower number say 1024 if you only have 1 GB available.